import { Component } from '@angular/core';

@Component({
  selector: 'fnm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'principalPage';
}
